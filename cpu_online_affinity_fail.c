/*
 * Simple reproducer to reveal the issue with sched_setaffinity() targeting
 * recently onlined CPU after netlink "cpu online" event is received.
 * Initially written by Oleksandr Natalenko.
 * Re-written a bit to remove pthread usage and to make the code smaller
 * (and simplier).
 *
 * Compile: gcc -o cpu_online_affinity_fail -Wall cpu_online_affinity_fail.c -ludev
 * (might need smth like libudev-dev package)
 *
 * By default, it plays with CPU 1. If you want to change this --
 * check ONLINE_CMD and OFFLINE_CMD defines.
 *
 */

#define _GNU_SOURCE

#include <errno.h>
#include <libudev.h>
#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sysexits.h>
#include <unistd.h>

#define panic(A)		__panic(__LINE__, errno, A)
#define EPOLL_MAXEVENTS		1
#define ONLINE_CMD		"echo 1 > /sys/devices/system/cpu/cpu1/online"
#define OFFLINE_CMD		"echo 0 > /sys/devices/system/cpu/cpu1/online"

struct udev_mon_local {
	struct udev *udev;
	struct udev_monitor *mon;
};

static unsigned long counter;

__attribute__((noreturn)) static void __panic(int _line, int _errno, const char* _routine)
{
	fprintf(stderr, "iter=%lu, line: %d, routine: %s, error: %s(%i)\n",
				counter, _line, _routine, strerror(_errno), _errno);
	exit(EX_SOFTWARE);
}

static int udev_monitor_prepare(struct udev_mon_local *udev_struct)
{
	udev_struct->udev = udev_new();
	if (!udev_struct->udev)
		panic("udev_new");

	udev_struct->mon = udev_monitor_new_from_netlink(udev_struct->udev, "udev");
	if (!udev_struct->mon)
		panic("udev_monitor_new_from_netlink");

	if (udev_monitor_filter_add_match_subsystem_devtype(udev_struct->mon,
							"cpu", NULL) < 0)
		panic("udev_monitor_filter_add_match_subsystem_devtype");

	if (udev_monitor_enable_receiving(udev_struct->mon) < 0)
		panic("udev_monitor_enable_receiving");

	return udev_monitor_get_fd(udev_struct->mon);
}

int main(int _argc, char** _argv)
{
	struct epoll_event events[EPOLL_MAXEVENTS];
	struct epoll_event event;
	const char *online = ONLINE_CMD;
	const char *offline = OFFLINE_CMD;
	unsigned long cpu = ULONG_MAX;
	cpu_set_t cpumask;
	struct udev_device *dev = NULL;
	struct udev_mon_local udev_struct = {
		.udev = NULL,
		.mon = NULL,
	};
	const char *action = NULL;
	int mon_fd = -1;
	int poll_fd = -1;
	int n_poll_events = -1;
	size_t i = 0;
	(void)_argc;

	/* Try to online CPU 1, needed to make while() loop work */
	system(online);
	usleep(50000);

	memset(&event, 0, sizeof event);
	memset(events, 0, EPOLL_MAXEVENTS * (sizeof event));
	memset(&cpumask, 0, sizeof cpumask);

	mon_fd = udev_monitor_prepare(&udev_struct);
	if (mon_fd < 0)
		panic("udev_monitor_get_fd");

	poll_fd = epoll_create1(0);
	if (poll_fd == -1)
		panic("epoll_create1");

	event.data.fd = mon_fd;
	event.events = EPOLLIN;
	if (epoll_ctl(poll_fd, EPOLL_CTL_ADD, mon_fd, &event) == -1)
		panic("epoll_ctl");

	memset(&event, 0, sizeof event);

	while (true) {
		/*
		 * Offline or online CPU on every iteration.
		 * When "online" event is received then try to sched_setaffinity();
		 * on "offline" event just do nothing.
		 */
		if (counter & 1)
			system(online);
		else
			system(offline);

		counter++;

		n_poll_events = epoll_wait(poll_fd, events, EPOLL_MAXEVENTS, -1);
		if (n_poll_events > 0) {
			for (i = 0; i < n_poll_events; i++) {
				if (events[i].events == EPOLLIN) {
					dev = udev_monitor_receive_device(udev_struct.mon);
					if (!dev)
						panic("udev_monitor_receive_device");

					action = udev_device_get_action(dev);
					cpu = strtoul(udev_device_get_sysnum(dev), 0, 10);
					if (cpu == ULONG_MAX)
						panic("strtoul");

					if (strncmp(action, "online", strlen("online")) == 0) {
						printf("CPU %lu went online\n", cpu);

						/* Online event received, let's do setaffinity on it */
						CPU_ZERO(&cpumask);
						CPU_SET(cpu, &cpumask);
						if (sched_setaffinity(0, sizeof cpumask, &cpumask) != 0)
							panic("sched_setaffinity");

					} else if (strncmp(action, "offline", strlen("offline")) == 0)
						printf("CPU %lu went offline\n", cpu);
					else
						panic("action");
				}
			}
		} else
			panic("epoll_wait");
	}

	exit(EX_OK);
}
